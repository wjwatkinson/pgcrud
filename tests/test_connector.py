import pytest
from pgcrud.connector import Connector

class TestConnector:

    def test__find_val_pos(self):
        criteria = 'WHERE a = %s AND b = %s AND c = %s'
        conn = Connector('test', 'test', 'test', 'test')
        assert conn._find_val_pos(criteria, 1) == 21

    def test__flatten_values(self):
        l = ['a', 'abc', ['avd', 'aml'], 'cdc', ['mlm']]
        conn = Connector('test', 'test', 'test', 'test')
        assert conn._flatten_values(l) == ['a', 'abc', 'avd', 'aml', 'cdc', 'mlm']

    def test__replace_criteria(self):
        criteria = 'WHERE a = %s AND b in %s AND c = %s'
        conn = Connector('test', 'test', 'test', 'test')
        expected = 'WHERE a = %s AND b in (%s,%s,%s) AND c = %s'
        assert  conn._replace_criteria(criteria, 1, 3) == expected

    def test__in_criteria(self):
        criteria = 'WHERE a = %s AND b in %s AND e = %s'
        values = ['am', ['be', 'c', 'd'], 3]
        conn = Connector('test', 'test', 'test', 'test')
        c, v = conn._in_criteria(criteria, values)
        assert c == 'WHERE a = %s AND b in (%s,%s,%s) AND e = %s'
        assert v == ['am', 'be', 'c', 'd', 3]

    def test__in_criteria_none(self):
        criteria = None
        values = None
        conn = Connector('test', 'test', 'test', 'test')
        c, v = conn._in_criteria(criteria, values)
        assert c == None
        assert v == None

    def test__in_criteria_no_criteria(self):
        criteria = None
        values = ['a']
        conn = Connector('test', 'test', 'test', 'test')
        try:
            c, v = conn._in_criteria(criteria, values)
        except Exception as e:
            assert str(e) == 'Criteria are require to set criteria values'
    
    def test__in_criteria_2(self):
        criteria = 'WHERE a = %s AND b in %s AND e in %s'
        values = ['a', ['b', 'c', 'd'], ['e']]
        conn = Connector('test', 'test', 'test', 'test')
        c, v = conn._in_criteria(criteria, values)
        assert c == 'WHERE a = %s AND b in (%s,%s,%s) AND e in (%s)'
        assert v == ['a', 'b', 'c', 'd', 'e']

    def test__row_to_dict(self):
        row = ['a', 'b', 'c']
        fields = ['f', 's', 't']
        expected = {'f': 'a', 's': 'b', 't':'c'}
        conn = Connector('test', 'test', 'test', 'test')
        assert conn._row_to_dict(fields, row) == expected

    def test__dict_to_up(self):
        data = {'a': 1,
                'b': 2,
                'c': 'c'}
        conn = Connector('test', 'test', 'test', 'test')
        u, v = conn._dict_to_up(data)
        exp_u = 'a = %s,b = %s,c = %s'
        exp_v = [1, 2, 'c']
        assert u == exp_u
        assert v == exp_v

    def test__dict_to_cr(self):
        data = {'a': 1,
                'b': 2,
                'c': 'c'}
        conn = Connector('test', 'test', 'test', 'test')
        f, var, val = conn._dict_to_cr(data)
        exp_f = 'a,b,c'
        exp_val = [1, 2, 'c']
        exp_var = '%s,%s,%s'
        assert f == exp_f
        assert val == exp_val
        assert var == exp_var